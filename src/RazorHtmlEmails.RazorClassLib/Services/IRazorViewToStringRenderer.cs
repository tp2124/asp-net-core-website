
using System.Threading.Tasks;

namespace RazorHtmlEmails.RazorClassLib.Services
{
    public interface IRazorViewToStringRenderer
    {
        Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model);
    }
}