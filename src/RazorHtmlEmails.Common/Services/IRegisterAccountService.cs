using System.Threading.Tasks;

namespace RazorHtmlEmails.Common.Services
{
    public interface IRegisterAccountService
    {
        Task Register(string email, string baseUrl);
    }
}