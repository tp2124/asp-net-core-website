﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using NETCoreWebsite.Models;
using RazorHtmlEmails.Common.Services;
using System;
using System.Diagnostics;

namespace NETCoreWebsite.Controllers
{
    public class HomeController : Controller
    {
        private IRegisterAccountService _accountService;
        private IConfiguration _appConfig;
        private IHostingEnvironment _env;

        public HomeController(IRegisterAccountService accountService, IConfiguration config, IHostingEnvironment env)
        {
            _accountService = accountService;
            _appConfig = config;
            _env = env;
        }

        public IActionResult Index()
        {
            // Messing around with accessing configuration loaded parameters.
            // See https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.2
            if (_env.IsDevelopment())
            {
                foreach (IConfigurationSection kvp in _appConfig.GetSection("CustomSettings").GetChildren())
                {
                    Console.WriteLine(kvp);
                }
                bool deepValue = _appConfig.GetValue<bool>("CustomSettings:SecondLevelSection:DeepConfig");
            }
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult RazorTemplate()
        {

            _accountService.Register("fakeMcFakeEmail@notreal.com", "fakeURIBase");
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
